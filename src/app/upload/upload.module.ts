import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UploadRoutingModule } from './upload-routing.module';
import { ShowlistComponent } from './showlist/showlist.component';

@NgModule({
  imports: [
    CommonModule,
    UploadRoutingModule,
    
  ],
  declarations: [ShowlistComponent]
})
export class UploadModule { }
