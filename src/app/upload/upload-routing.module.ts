import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShowlistComponent } from './showlist/showlist.component';

const routes: Routes = [
  // { path: 'fixed', component: FixedTableComponent , data: { animation: 'fixed' }},
  	// { path: 'featured', component: FeatureTableComponent ,data: { animation: 'featured' }},
  	{ path: 'uploadcsv', component: ShowlistComponent ,data: { animation: 'responsive' }}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UploadRoutingModule { }
